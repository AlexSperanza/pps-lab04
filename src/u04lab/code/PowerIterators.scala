package u04lab.code

import u04lab.code.Lists.List.{Cons, append}
import u04lab.code.Lists._
import u04lab.code.Optionals._
import u04lab.code.Streams.Stream
import u04lab.code.Streams.Stream.take

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = PowerIterator(Streams.Stream.iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = PowerIterator(List.toStream(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = PowerIterator(take(Streams.Stream.generate(Math.random().round.toInt == 1))(size))
}

object PowerIterator {

  def apply[A](stream: Streams.Stream[A]):PowerIterator[A] = PowerIteratorImpl[A](stream)

  private case class PowerIteratorImpl[A](var stream: Streams.Stream[A]) extends PowerIterator[A] {

    private var extractedValues: List[A] = List.nil

    override def next(): Option[A] = stream match {
      case Stream.Cons(head, tail) => {
        stream = tail()
        extractedValues = append(extractedValues, Cons(head(), Lists.List.nil))
        Optionals.Option.Some(head())
      }
      case Stream.Empty() => Optionals.Option.None()
    }

    override def allSoFar(): List[A] = extractedValues

    override def reversed(): PowerIterator[A] = new PowerIteratorsFactoryImpl().fromList(Lists.List.reverse(extractedValues))
  }
}