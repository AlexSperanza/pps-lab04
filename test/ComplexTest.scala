import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u04lab.code.Complex

class ComplexTest {

  @Test
  def testComplex(): Unit = {
    val c1 = Complex(1.0, 5.0)
    val c2 = Complex(2.0, 0.0)
    assertEquals(Complex(3.0, 5.0), c1 + c2)
  }
}
