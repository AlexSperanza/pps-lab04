package u04lab.code

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import u04lab.code.Lists.List.{Cons, nil}
import u04lab.code.Lists.sameTeacher

class OptionalExerciseTest {

  @Test
  def testVariadicListCreation(): Unit = {
    val list = Lists.List(1, 2, 3)
    assertEquals(Cons(1, Cons(2, Cons(3, nil))), list)
  }

  @Test
  def testVariadicEmptyList(): Unit = {
    val list = Lists.List()
    assertEquals(nil, list)
  }

  @Test
  def testSameTeacherExtractor(): Unit = {
    val cPPS = Course("PPS", "Mirko Viroli")
    val cOOP = Course("OOP", "Mirko Viroli")
    val cPCD = Course("PCD", "Alessandro Ricci")
    val listSame = Lists.List(cPPS, cOOP)
    val listDifferent = Lists.List(cPCD, cPPS)

    assertEquals(s"$listSame have same teacher Mirko Viroli" , listSame match {
      case sameTeacher (t) => s"$listSame have same teacher $t"
      case _ => s"$listSame have different teachers"
    })

    assertEquals(s"$listDifferent have different teachers" , listDifferent match {
      case sameTeacher ( t ) => s"$listDifferent have same teacher $t"
      case _ => s"$listDifferent have different teachers"
    })

  }
}
