package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental() {
    val pi = factory.incremental(5,_+2) // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next())
    assertEquals(Option.of(7), pi.next())
    assertEquals(Option.of(9), pi.next())
    assertEquals(Option.of(11), pi.next())
    assertEquals(List.Cons(5, List.Cons(7, List.Cons(9, List.Cons(11,List.Nil())))), pi.allSoFar()) // elementi già prodotti
    for (i <- 0 until 10) {
      pi.next() // procedo in avanti per un po'..
    }
    assertEquals(Option.of(33), pi.next()) // sono arrivato a 33
  }

    @Test
    def testRandom() { // semi-automatico, si controlleranno anche le stampe a video
    	val pi = factory.randomBooleans(4) // pi produce 4 booleani random
    	val b1 = pi.next() match {
        case Option.Some(a) => a
      }
    	val b2 = pi.next() match {
        case Option.Some(a) => a
      }
    	val b3 = pi.next() match {
        case Option.Some(a) => a
      }
    	val b4 = pi.next() match {
        case Option.Some(a) => a
      }
    	println(b1+ " "+b2+ " "+b3+ " "+b4) // stampo a video.. giusto per vedere se sono proprio random..
      assertTrue(Option.isEmpty(pi.next())) // ne ho già prodotti 4, quindi il prossimo è un opzionale vuoto
      assertEquals(pi.allSoFar(), Cons(b1, Cons(b2, Cons(b3, Cons(b4, nil))))) // ho prodotto proprio b1,b2,b3,b4
    }

    @Test
    def testFromList() {
    	val pi = factory.fromList(Cons("a", Cons("b", Cons("c", nil)))) // pi produce a,b,c
    	assertEquals(pi.next(), Option.Some("a"))
      assertEquals(pi.next(),Option.Some("b"))
      assertEquals(pi.allSoFar(),Cons("a", Cons("b", nil))) // fin qui a,b
      assertEquals(pi.next(),Option.Some("c"))
      assertEquals(pi.allSoFar(),Cons("a", Cons("b", Cons("c", nil)))) // fin qui a,b,c
      assertTrue(Option.isEmpty(pi.next())) // non c'è più niente da produrre
    }

    @Test
    def optionalTestReversedOnList() {
    	val pi = factory.fromList(Cons("a", Cons("b", Cons("c", nil))))
    	assertEquals(pi.next(),Option.Some("a"))
      assertEquals(pi.next(),Option.Some("b"))
      val pi2 = pi.reversed() //pi2 itera su b,a
      assertEquals(pi.next(),Option.Some("c")) // c viene prodotto da pi normalmente
      assertTrue(Option.isEmpty(pi.next()))

      assertEquals(pi2.next(),Option.Some("b"))
      assertEquals(pi2.next(),Option.Some("a"))
      assertEquals(pi2.allSoFar(), Cons("b", Cons("a", nil))) // pi2 ha prodotto b,a
      assertTrue(Option.isEmpty(pi2.next()))
    }

    @Test
    def optionalTestReversedOnIncremental() {
    	val pi = factory.incremental(0,_+1) // 0,1,2,3,...
    	assertEquals(pi.next(),Option.Some(0))
      assertEquals(pi.next(),Option.Some(1))
      assertEquals(pi.next(),Option.Some(2))
      assertEquals(pi.next(),Option.Some(3))
      val pi2 = pi.reversed() // pi2 itera su 3,2,1,0
      assertEquals(pi2.next(),Option.Some(3))
      assertEquals(pi2.next(),Option.Some(2))
      val pi3 = pi2.reversed() // pi2 ha prodotto 3,2 in passato, quindi pi3 itera su 2,3
      assertEquals(pi3.next(),Option.Some(2))
      assertEquals(pi3.next(),Option.Some(3))
      assertTrue(Option.isEmpty(pi3.next()))
    }

}